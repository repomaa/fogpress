FROM jreinert/crystal-alpine:latest AS build
WORKDIR /app
COPY shard.yml shard.lock ./
RUN shards install --production
COPY . ./
RUN shards build --static --release --production -Dmusl

FROM scratch AS final-image
COPY --from=build /etc/ssl /etc/ssl
COPY --from=build /app/bin/fogpress /usr/bin/fogpress
CMD ["/usr/bin/fogpress"]
