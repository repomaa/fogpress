require "mqtt_crystal"
require "fogtrack"

module Fogpress
  class Watcher
    def initialize
      mqtt_url = URI.parse("mqtt://#{ENV["MQTT_HOST"]}")
      ENV["MQTT_PORT"]?.try { |port| mqtt_url.port = port.to_i }
      mqtt_url.user = ENV["MQTT_USERNAME"]
      mqtt_url.password = ENV["MQTT_PASSWORD"]
      @mqtt_client = MqttCrystal::Client.new(url: mqtt_url.to_s)

      @mqtt_client.subscribe("homie/#{ENV["DEVICE_ID"]}/#")
      @tracking_tick = Channel(Nil).new(1)
    end

    def watch
      spawn tracking_state
      @mqtt_client.get("homie/#{ENV["DEVICE_ID"]}/#") do |topic, message|
        if topic.ends_with?("/switch/pressed") && message == "true"
          Fogtrack.tracking? ? Fogtrack.stop_tracking : Fogtrack.resume_tracking
          @tracking_tick.send(nil)
        end
      end
    end

    private def tracking_state
      spawn tracking_tick

      loop do
        state = Fogtrack.tracking?
        @mqtt_client.publish("homie/#{ENV["DEVICE_ID"]}/led/on/set", state.to_s)
        @tracking_tick.receive
      end
    end

    private def tracking_tick
      loop do
        @tracking_tick.send(nil)
        sleep 10
      end
    end
  end
end

watcher = Fogpress::Watcher.new
watcher.watch
